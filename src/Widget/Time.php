<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Time extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Time();
    }

    public function valueFormToObject($value)
    {
        return $value ? new \DateTime($value) : null;
    }

    public function valueObjectToForm($value)
    {
        return $value instanceof \DateTime
            ? $value->format('H:i:s')
            : null;
    }
}
