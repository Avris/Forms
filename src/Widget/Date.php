<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Date extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Date();
    }

    public function valueFormToObject($value)
    {
        return $value ? new \DateTime($value) : null;
    }

    public function valueObjectToForm($value)
    {
        return $value instanceof \DateTime
            ? $value->format('Y-m-d')
            : null;
    }
}
