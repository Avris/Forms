<?php
namespace Avris\Forms\Widget;

class Checkbox extends Widget
{
    protected $data = false;

    public function setData($data)
    {
        $this->data = $data === null ? false : (bool) $data;
    }
}
