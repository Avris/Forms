<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Url extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Url();
    }
}
