<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;
use Avris\Forms\Form;

class Csrf extends Hidden implements DontMapOnObject
{
    /** @var string */
    private $token;

    public function initialise(array $options, array $asserts = []): Widget
    {
        $this->token = $options['token'];
        $this->setData($this->token);

        return parent::initialise($options, $asserts);
    }

    public function bindObject($value)
    {
    }

    protected function getDefaultAssert()
    {
        return new Assert\Csrf($this->token);
    }
}
