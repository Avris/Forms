<?php
namespace Avris\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Forms\Assert as Assert;

class Choice extends Widget
{
    /** @var array */
    protected $choices = [];

    public function initialise(array $options, array $asserts = []): Widget
    {
        $keysCallback = $options['keys'] ?? function ($value, $key) {
            return (string) $key;
        };

        $labelsCallback = $options['labels'] ?? function ($value, $key) {
            return (string) $value;
        };

        foreach ($options['choices'] ?? [] as $key => $value) {
            $this->choices[$keysCallback($value, $key)] = [
                'value' => $value,
                'label' => $labelsCallback($value, $key),
            ];
        }

        parent::initialise($options, $asserts);

        return $this;
    }

    public function getChoices()
    {
        $choices = $this->choices;
        if ($this->getOption('add_empty')) {
            $choices = ['' => ['value' => null, 'label' => '']] + $choices;
        }

        return $choices;
    }

    protected function getDefaultAssert()
    {
        return new Assert\Choice(
            $this->choices,
            (bool) $this->getOption('multiple')
        );
    }

    public function valueFormToObject($value)
    {
        $keysCallback = $this->getOption('keys');

        if ($this->getOption('multiple')) {
            $arrayValue = [];
            foreach (BagHelper::toArray($value) as $singleValue) {
                $arrayValue[] = $keysCallback
                    ? $this->choices[$singleValue]['value'] ?? null
                    : $singleValue;
            }

            return $arrayValue;
        }

        return $keysCallback
            ? $this->choices[$value]['value'] ?? null
            : $value;
    }

    public function valueObjectToForm($value)
    {
        if ($this->getOption('multiple')) {
            $arrayValue = [];
            foreach (BagHelper::toArray($value) as $singleValue) {
                $arrayValue[] = $this->singleValueObjectToForm($singleValue);
            }

            return $arrayValue;
        }

        return $this->singleValueObjectToForm($value);
    }

    private function singleValueObjectToForm($value)
    {
        if ($value === null) {
            return '';
        }

        if ($keysCallback = $this->getOption('keys')) {
            return $keysCallback($value);
        }

        return $value;
    }

    public function isSelected(string $key)
    {
        return $this->getOption('multiple')
            ? in_array($key, $this->data ?? [], true)
            : $key === $this->data;
    }
}
