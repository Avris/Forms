<?php
namespace Avris\Forms\Widget;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert as Assert;

class File extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\File\File();
    }

    public function setData($data)
    {
        if ($data !== null && !$data instanceof UploadedFile) {
            throw new \InvalidArgumentException(sprintf(
                'The File widget must be bound with an instance of %s',
                UploadedFile::class
            ));
        }

        return parent::setData($data);
    }
}
