<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Number extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Number();
    }
}
