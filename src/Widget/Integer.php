<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Integer extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Integer();
    }
}
