<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;

class Email extends Widget
{
    protected function getDefaultAssert()
    {
        return new Assert\Email();
    }

    public function valueFormToObject($value)
    {
        return $value ? strtolower(trim($value)) : null;
    }
}
