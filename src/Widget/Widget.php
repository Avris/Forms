<?php
namespace Avris\Forms\Widget;

use Avris\Forms\Assert as Assert;
use Avris\Bag\Bag;

abstract class Widget
{
    protected $data;

    /** @var Bag */
    protected $options;

    /** @var Assert\Assert[] */
    private $asserts;

    /** @var string[] */
    protected $errors = [];

    public function initialise(array $options, array $asserts = []): self
    {
        $this->options = new Bag($options);

        if ($this->options->has('default')) {
            $this->setData($this->options->get('default'));
        }

        if ($this->options->has('prototype')) {
            $this->bindObject($this->options->get('prototype'));
        }

        $this->setAsserts($asserts);

        return $this;
    }

    protected function setAsserts(array $asserts)
    {
        $this->asserts = $this->manageAsserts($asserts);

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    protected function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function buildObject($object = null)
    {
        return $this->valueFormToObject($this->getData());
    }

    public function bindObject($value)
    {
        $this->setData($this->valueObjectToForm($value));

        return $this;
    }

    protected function valueFormToObject($value)
    {
        return $value;
    }

    protected function valueObjectToForm($value)
    {
        return $value;
    }

    public function isValid(): bool
    {
        $this->errors = [];
        foreach ($this->asserts as $assert) {
            if (Assert\Assert::isEmpty($this->data) && !$assert instanceof Assert\IsRequired) {
                continue;
            }
            if (!$assert->validate($this->data)) {
                $this->errors[] = ['message' => $assert->getMessage(), 'replacements' => $assert->getReplacements()];
            }
        }

        return count($this->errors) === 0;
    }

    /**
     * @return Assert\Assert[]
     */
    public function getAsserts(): array
    {
        return $this->asserts;
    }

    /**
     * @return Assert\Assert|null
     */
    protected function getDefaultAssert()
    {
        return null;
    }

    protected function manageAsserts($asserts)
    {
        $defaultAssert = $this->getDefaultAssert();
        if (!$defaultAssert) {
            return $asserts;
        }

        $defaultAssertClass = get_class($defaultAssert);
        $useDefault = true;
        foreach ($asserts as $assert) {
            if ($assert instanceof $defaultAssertClass) {
                $useDefault = false;
            }
        }
        if ($useDefault) {
            $asserts[] = $defaultAssert;
        }

        return $asserts;
    }

    public function isRequired(): bool
    {
        foreach ($this->asserts as $assert) {
            if ($assert instanceof Assert\IsRequired) {
                return true;
            }
        }

        return false;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getOption($name, $default = null)
    {
        if ($this->options === null) {
            throw new \RuntimeException('You need to execute ::initialise() before you use the widget');
        }

        return $this->options->get($name, $default);
    }

    public function isReadonly()
    {
        return (bool) $this->options->get('readonly', false);
    }
}
