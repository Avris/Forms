<?php
namespace Avris\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;

class Multiple extends Widget
{
    /** @var Widget[] */
    protected $data = [];

    /** @var Widget */
    private $prototype;

    /** @var WidgetFactory */
    private $widgetFactory;

    public function __construct(WidgetFactory $widgetFactory)
    {
        $this->widgetFactory = $widgetFactory;
    }

    public function initialise(array $options, array $asserts = []): Widget
    {
        if (isset($options['prototype'])) {
            $options['element_prototype'] = $options['prototype'];
            unset($options['prototype']);
        }

        if (!isset($options['add'])) {
            $options['add'] = true;
        }

        if (!isset($options['remove'])) {
            $options['remove'] = true;
        }

        parent::initialise($options, $asserts);

        $this->prototype = $this->buildWidget();

        return $this;
    }

    private function buildWidget()
    {
        /** @var Widget $widget */
        $widget = $this->widgetFactory->build(
            $this->getOption('widget'),
            $this->getOption('element_options', []) + [ 'csrf' => false ],
            $this->getOption('element_asserts', [])
        );

        if ($prototype = $this->getOption('element_prototype')) {
            $widget->bindObject(clone $prototype);
        }

        return $widget;
    }

    public function getPrototype(): Widget
    {
        return $this->prototype;
    }

    public function getColumnCount()
    {
        $columnsCount = $this->prototype instanceof Form
            ? array_reduce($this->prototype->all(), function ($carry, array $item) {
                return $carry + count($item);
            })
            : 1;

        if (!$this->getOption('remove')) {
            $columnsCount--;
        }

        return $columnsCount;
    }

    public function setData($data)
    {
        $newData = [];

        foreach (BagHelper::toArray($data) as $i => $object) {
            $newData[$i] = $this->getWidget($i)->setData($object);
        }

        $this->data = $newData;

        return $this;
    }

    public function isValid(): bool
    {
        $valid = parent::isValid();

        foreach ($this->data as $i => $widget) {
            $valid = $widget->isValid(true) && $valid;
        }

        return $valid;
    }

    public function buildObject($object = null)
    {
        $out = [];

        foreach ($this->data as $i => $widget) {
            $out[] = $widget->buildObject();
        }

        return $out;
    }

    public function bindObject($value)
    {
        $this->data = [];

        foreach (BagHelper::toArray($value) as $i => $object) {
            $this->data[$i] = $this->getWidget($i)->bindObject($object);
        }

        return $this;
    }

    private function getWidget($id): Widget
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }

        return $this->data[$id] = $this->buildWidget();
    }

    public function canRemove(Widget $widget): bool
    {
        $canRemove = $this->getOption('remove');

        return is_callable($canRemove)
            ? $canRemove($widget->buildObject())
            : $canRemove;
    }

    public function getData()
    {
        $value = [];

        foreach ($this->data as $widget) {
            $value[] = $widget->getData();
        }

        return $value;
    }

    public function getSubwidgets()
    {
        return $this->data;
    }
}
