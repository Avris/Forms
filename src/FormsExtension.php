<?php
namespace Avris\Forms;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;
use Avris\Forms\Security\CsrfProvider;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\Widget as Widget;
use Avris\Http\Session\Session;
use Avris\Http\Session\SessionInterface;
use Avris\Localisator\Transformer\FallbackToPatternTransformer;

/**
 * @codeCoverageIgnore
 */
final class FormsExtension implements ContainerBuilderExtension
{
    const SIMPLE_WIDGETS = [
        Widget\Checkbox::class,
        Widget\Choice::class,
        Widget\Csrf::class,
        Widget\Custom::class,
        Widget\Date::class,
        Widget\DateTime::class,
        Widget\Email::class,
        Widget\File::class,
        Widget\Hidden::class,
        Widget\Integer::class,
        Widget\Number::class,
        Widget\NumberAddon::class,
        Widget\Password::class,
        Widget\Text::class,
        Widget\TextAddon::class,
        Widget\Textarea::class,
        Widget\Time::class,
        Widget\Url::class,
    ];

    /** @var array */
    private $forms;

    /** @var array */
    private $widgets;

    /**
     * @param array $forms [ className => [ constructor arguments ] ]
     * @param array $widgets [ className => [ constructor arguments ] ]
     */
    public function __construct(array $forms = [], array $widgets = [])
    {
        $this->forms = $forms;
        $this->widgets = $widgets;
    }

    public function extend(ContainerInterface $container)
    {
        $container->setDefinition(WidgetFactory::class, [
            'arguments' => [
                '$container' => '@',
            ],
            'public' => true,
        ]);

        $container->setDefinition(CsrfProviderInterface::class, CsrfProvider::class);
        $container->setDefinition(CsrfProvider::class, [
            'arguments' => [
                '$session' => '@' . SessionInterface::class,
            ]
        ]);

        $container->setDefinition(SessionInterface::class, Session::class);
        $container->setDefinition(Session::class, ['public' => true]);

        foreach (self::SIMPLE_WIDGETS as $widgetClass) {
            $container->setDefinition($widgetClass, ['public' => true, 'factory' => true]);
        }

        foreach ($this->widgets as $widgetClass => $arguments) {
            $container->setDefinition($widgetClass, $arguments + ['public' => true, 'factory' => true]);
        }

        $container->setDefinition(Widget\Multiple::class, [
            'public' => true,
            'factory' => true,
            'arguments' => [
                '$widgetFactory' => '@' . WidgetFactory::class,
            ],
        ]);

        foreach ($this->forms as $formClass => $arguments) {
            $container->setDefinition($formClass, [
                'public' => true,
                'factory' => true,
                'arguments' => $arguments + [
                    '$widgetFactory' => '@' . WidgetFactory::class,
                    '$csrfProvider' => '@' . CsrfProviderInterface::class,
                ],
            ]);
        }

        $container->setDefinition(FallbackToPatternTransformer::class, [
            'arguments' => [
                '$patterns' => ['entity' => ['^.*\.fields.(.*)$']],
            ],
            'tags' => ['translationTransformer', 'subscriber'],
        ]);
    }
}
