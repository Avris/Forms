<?php
namespace Avris\Forms\Assert;

final class Choice extends Assert
{
    /** @var array */
    private $choices;

    /** @var bool */
    private $multiple;

    public function __construct(array $choices, bool $multiple, ?string $message = null)
    {
        $this->choices = $choices;
        $this->multiple = $multiple;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        if (!$this->multiple && is_array($value)) {
            return false;
        }

        if ($this->multiple && !is_array($value)) {
            return false;
        }

        if (!is_array($value)) {
            $value = [$value];
        }

        $allowed = array_keys($this->choices);
        foreach ($value as $singleValue) {
            if (!in_array($singleValue, $allowed)) {
                return false;
            }
        }

        return true;
    }
}
