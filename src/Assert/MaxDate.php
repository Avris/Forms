<?php
namespace Avris\Forms\Assert;

final class MaxDate extends Assert
{
    private $max;

    public function __construct($max, ?string $message = null)
    {
        $this->max = $max instanceof \DateTime ? $max : new \DateTime($max);
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        if (!$value instanceof \DateTime) {
            $value = new \DateTime($value);
        }

        return $value <= $this->max;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->max->format('Y-m-d')];
    }
}
