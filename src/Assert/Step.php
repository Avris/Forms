<?php
namespace Avris\Forms\Assert;

final class Step extends Assert
{
    private $step;

    public function __construct($step, ?string $message = null)
    {
        $this->step = $step;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        $value = (float) $value;

        return abs(($value / $this->step) - round($value / $this->step, 0)) < 0.0001;
    }

    public function getHtmlAttributes(): array
    {
        return ['step' => $this->step];
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->step];
    }
}
