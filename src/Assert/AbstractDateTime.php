<?php
namespace Avris\Forms\Assert;

abstract class AbstractDateTime extends Assert
{
    const FORMATS = [''];

    public function validate($value): bool
    {
        $value = str_replace('T', ' ', $value);

        try {
            $obj = new \DateTime($value);
        } catch (\Exception $e) {
            return false;
        }

        foreach (static::FORMATS as $format) {
            if ($obj->format($format) === $value) {
                //dump($format, $obj->format($format), $value, '----');
                return true;
            }
        }

        return false;
    }
}
