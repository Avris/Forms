<?php
namespace Avris\Forms\Assert;

final class MinDate extends Assert
{
    private $min;

    public function __construct($min, ?string $message = null)
    {
        $this->min = $min instanceof \DateTime ? $min : new \DateTime($min);
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        if (!$value instanceof \DateTime) {
            $value = new \DateTime($value);
        }

        return $value >= $this->min;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->min->format('Y-m-d')];
    }
}
