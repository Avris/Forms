<?php
namespace Avris\Forms\Assert;

final class Csrf extends Assert implements IsRequired
{
    /** @var string */
    private $token;

    public function __construct(string $token, ?string $message = null)
    {
        $this->token = $token;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return $value === $this->token;
    }
}
