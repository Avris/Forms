<?php
namespace Avris\Forms\Assert;

final class Contains extends Assert
{
    /** @var string */
    private $required;

    public function __construct($required, ?string $message = null)
    {
        $this->required = $required;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return mb_strpos($value, $this->required) !== false;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->required];
    }
}
