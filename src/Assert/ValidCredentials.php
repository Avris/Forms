<?php
namespace Avris\Forms\Assert;

use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;

final class ValidCredentials extends Assert implements IsRequired
{
    /** @var callable */
    private $fetchUserCallback;

    /** @var CryptInterface */
    private $crypt;

    public function __construct(callable $fetchUserCallback, CryptInterface $crypt, ?string $message = null)
    {
        $this->fetchUserCallback = $fetchUserCallback;
        $this->crypt = $crypt;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        /** @var UserInterface $user */
        $user = call_user_func($this->fetchUserCallback);

        if (!$user) {
            return false;
        }

        foreach ($user->getAuthenticators(SecurityManager::AUTHENTICATOR_PASSWORD) as $auth) {
            if ($this->crypt->passwordVerify($value, $auth->getPayload())) {
                if ($this->crypt->passwordNeedsRehash($auth->getPayload())) {
                    $auth->invalidate();
                    $user->createAuthenticator(
                        SecurityManager::AUTHENTICATOR_PASSWORD,
                        $this->crypt->passwordHash($value)
                    );
                }

                return true;
            }
        }

        return false;
    }
}
