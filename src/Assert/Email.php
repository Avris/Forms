<?php
namespace Avris\Forms\Assert;

final class Email extends Assert
{
    public function validate($value): bool
    {
        return filter_var(strtolower(trim($value)), FILTER_VALIDATE_EMAIL);
    }
}
