<?php
namespace Avris\Forms\Assert;

final class Date extends AbstractDateTime
{
    const FORMATS = ['Y-m-d'];

    public function getHtmlAttributes(): array
    {
        return ['pattern' => '^\d{4}-\d{2}-\d{2}$'];
    }
}
