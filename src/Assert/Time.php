<?php
namespace Avris\Forms\Assert;

final class Time extends AbstractDateTime
{
    const FORMATS = ['H:i', 'H:i:s'];

    public function getHtmlAttributes(): array
    {
        return ['pattern' => '^\d{2}:\d{2}$'];
    }
}
