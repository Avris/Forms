<?php
namespace Avris\Forms\Assert;

final class Regexp extends Assert
{
    /** @var string */
    private $pattern;

    /** @var string */
    private $switches;

    public function __construct(string $pattern, string $switches = 'i', ?string $message = null)
    {
        $this->pattern = $pattern;
        $this->switches = $switches;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return preg_match('#'.$this->pattern.'#'.$this->switches, $value);
    }

    public function getHtmlAttributes(): array
    {
        return ['pattern' => $this->pattern];
    }

    public function getReplacements(): array
    {
        return ['%pattern%' => $this->pattern];
    }
}
