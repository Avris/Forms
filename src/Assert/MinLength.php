<?php
namespace Avris\Forms\Assert;

final class MinLength extends Assert
{
    private $min;

    public function __construct($min, ?string $message = null)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return mb_strlen($value) >= $this->min;
    }

    public function getHtmlAttributes(): array
    {
        return ['minLength' => $this->min];
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->min];
    }
}
