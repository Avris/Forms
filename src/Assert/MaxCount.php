<?php
namespace Avris\Forms\Assert;

final class MaxCount extends Assert
{
    /** @var int */
    private $max;

    public function __construct(int $max, ?string $message = null)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return count($value) <= $this->max;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->max];
    }
}
