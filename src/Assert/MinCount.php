<?php
namespace Avris\Forms\Assert;

final class MinCount extends Assert
{
    /** @var int */
    private $min;

    public function __construct(int $min, ?string $message = null)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return count($value) >= $this->min;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->min];
    }
}
