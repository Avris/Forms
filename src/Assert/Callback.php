<?php
namespace Avris\Forms\Assert;

final class Callback extends Assert
{
    /** @var callable */
    private $callback;

    public function __construct(callable $callback, ?string $message = null)
    {
        $this->callback = $callback;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return call_user_func($this->callback, $value);
    }
}
