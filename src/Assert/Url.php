<?php
namespace Avris\Forms\Assert;

final class Url extends Assert
{
    public function validate($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_URL);
    }
}
