<?php
namespace Avris\Forms\Assert;

abstract class Assert
{
    /** @var string|null */
    private $message;

    public function __construct(?string $message = null)
    {
        $this->message = $message;
    }

    abstract public function validate($value): bool;

    public function getHtmlAttributes(): array
    {
        return [];
    }

    public static function isEmpty($value): bool
    {
        return $value === "" || $value === null || $value === false || (is_array($value) && !count($value));
    }

    public function getName(): string
    {
        return str_replace('\\', '.', get_class($this));
    }

    public function getMessage(): string
    {
        return $this->message ?: 'assert:' . $this->getName();
    }

    public function getReplacements(): array
    {
        return [];
    }
}
