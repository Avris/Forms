<?php
namespace Avris\Forms\Assert;

final class NotBlank extends Assert implements IsRequired
{
    public function validate($value): bool
    {
        return !$this->isEmpty($value);
    }

    public function getHtmlAttributes(): array
    {
        return ['required' => true];
    }
}
