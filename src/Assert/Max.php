<?php
namespace Avris\Forms\Assert;

final class Max extends Assert
{
    private $max;

    public function __construct($max, ?string $message = null)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return $value <= $this->max;
    }

    public function getHtmlAttributes(): array
    {
        return ['max' => $this->max];
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->max];
    }
}
