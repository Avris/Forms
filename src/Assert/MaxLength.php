<?php
namespace Avris\Forms\Assert;

final class MaxLength extends Assert
{
    private $max;

    public function __construct($max, ?string $message = null)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        return mb_strlen($value) <= $this->max;
    }

    public function getHtmlAttributes(): array
    {
        return ['maxLength' => $this->max];
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->max];
    }
}
