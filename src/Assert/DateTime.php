<?php
namespace Avris\Forms\Assert;

final class DateTime extends AbstractDateTime
{
    const FORMATS = ['Y-m-d H:i', 'Y-m-d H:i:s'];

    public function getHtmlAttributes(): array
    {
        return ['pattern' => '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}$'];
    }
}
