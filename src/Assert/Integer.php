<?php
namespace Avris\Forms\Assert;

final class Integer extends Assert
{
    public function validate($value): bool
    {
        return is_numeric($value) && round($value) == $value;
    }
}
