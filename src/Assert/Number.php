<?php
namespace Avris\Forms\Assert;

final class Number extends Assert
{
    public function validate($value): bool
    {
        return is_numeric($value);
    }
}
