<?php
namespace Avris\Forms\Assert;

use Avris\Forms\Widget\Widget;

final class UniqueField extends Assert
{
    /** @var string */
    private $field;

    public function __construct(string $field, ?string $message = null)
    {
        $this->field = $field;
        parent::__construct($message);
    }

    public function validate($value): bool
    {
        $found = [];

        /** @var Widget $singleValue */
        foreach ($value as $singleValue) {
            $singleValue = $singleValue instanceof Widget ? $singleValue->getData() : $singleValue;
            if (empty($singleValue[$this->field])) {
                continue;
            }

            if (isset($found[$singleValue[$this->field]])) {
                return false;
            }

            $found[$singleValue[$this->field]] = true;
        }

        return true;
    }

    public function getReplacements(): array
    {
        return ['%field%' => $this->field];
    }
}
