<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class MaxSize extends Assert
{
    /** @var int */
    private $max;

    const UNITS = ['B', 'kB', 'MB', 'GB', 'TB'];

    const UNIT_PREFIXES = [
        '' => 0,
        'b' => 0,
        'k' => 1,
        'm' => 2,
        'g' => 3,
        't' => 4,
    ];

    public function __construct($max, ?string $message = null)
    {
        $this->max = $this->parseSize($max);
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        return $value->getSize() <= $this->max;
    }

    private function parseSize($size): int
    {
        if (is_int($size)) {
            return $size;
        }

        if (!preg_match('/^(\d+\.?\d*)\s*([kmgtb]?)b?$/i', $size, $matches)) {
            throw new \InvalidArgumentException(sprintf('Cannot parse file size "%s"', $size));
        }

        $power = self::UNIT_PREFIXES[strtolower($matches[2])];
        $bytes = (float) $matches[1] * pow(1024, $power);

        return (int) $bytes;
    }

    private function displaySize($size)
    {
        $i = -1;

        do {
            $oldSize = $size;
            $size = $oldSize / 1024;
            $i++;
        } while ($size > 0.1);

        return sprintf('%.1f %s', $oldSize, self::UNITS[$i]);
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->displaySize($this->max)];
    }
}
