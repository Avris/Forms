<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class Ratio extends Assert
{
    const HORIZONTAL = 'horizontal';
    const VERTICAL = 'vertical';
    const SQUARE = 'square';

    private $ratio;

    public function __construct(string $ratio, ?string $message = null)
    {
        $this->ratio = $ratio;
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        $imgsize = getimagesize($value->getTmpName());

        if ($this->ratio === self::HORIZONTAL && $imgsize[0] < $imgsize[1]) {
            return false;
        }

        if ($this->ratio === self::VERTICAL && $imgsize[0] > $imgsize[1]) {
            return false;
        }
        if ($this->ratio === self::SQUARE && $imgsize[0] !== $imgsize[1]) {
            return false;
        }

        return true;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->ratio];
    }
}
