<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class File extends Assert
{
    public function validate($value): bool
    {
        return $value instanceof UploadedFile && !$value->getError();
    }
}
