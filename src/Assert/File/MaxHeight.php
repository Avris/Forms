<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class MaxHeight extends Assert
{
    /** @var int */
    private $max;

    public function __construct(int $max, ?string $message = null)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        $imgsize = getimagesize($value->getTmpName());

        return $imgsize[1] <= $this->max;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->max];
    }
}
