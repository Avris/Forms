<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class MinHeight extends Assert
{
    /** @var int */
    private $min;

    public function __construct(int $min, ?string $message = null)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        $imgsize = getimagesize($value->getTmpName());

        return $imgsize[1] >= $this->min;
    }

    public function getReplacements(): array
    {
        return ['%value%' => $this->min];
    }
}
