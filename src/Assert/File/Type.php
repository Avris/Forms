<?php
namespace Avris\Forms\Assert\File;

use Avris\Bag\BagHelper;
use Avris\Bag\Set;
use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;
use Avris\Bag\Bag;

final class Type extends Assert
{
    /** @var Set|string[] */
    private $types;

    public function __construct($type, ?string $message = null)
    {
        $this->types = new Set($type, 'strtolower');
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        return $this->types->has($value->getClientMediaType());
    }
}
