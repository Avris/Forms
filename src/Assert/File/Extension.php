<?php
namespace Avris\Forms\Assert\File;

use Avris\Bag\BagHelper;
use Avris\Bag\Set;
use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class Extension extends Assert
{
    /** @var Set|string[] */
    private $extension;

    public function __construct($extension, ?string $message = null)
    {
        $this->extension = new Set(BagHelper::toArray($extension), 'strtolower');
        parent::__construct($message);
    }

    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        return $this->extension->has($value->getExtension());
    }
}
