<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\Assert;

final class Image extends Assert
{
    /**
     * @param UploadedFile $value
     */
    public function validate($value): bool
    {
        /** @var UploadedFile $value */
        return is_numeric(exif_imagetype($value->getTmpName()));
    }
}
