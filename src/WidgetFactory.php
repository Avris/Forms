<?php
namespace Avris\Forms;

use Avris\Forms\Widget as Widget;
use Avris\Container\ContainerInterface;

class WidgetFactory
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function build(
        string $type,
        array $options = [],
        $asserts = []
    ): Widget\Widget {
        $widget = $this->container->get($type);

        if (!$widget instanceof Widget\Widget) {
            throw new \InvalidArgumentException(sprintf(
                'Service "%s" should be an instance of %s to use it as a form widget',
                $type,
                Widget\Widget::class
            ));
        }

        return $widget->initialise($options, is_array($asserts) ? $asserts : [$asserts]);
    }
}
