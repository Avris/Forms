<?php
namespace Avris\Forms;

use Avris\Bag\BagHelper;

class Accessor
{
    /** @var object */
    protected $object;

    /**
     * @param object|null $object
     */
    public function __construct($object = null)
    {
        $this->object = $object ?: new \stdClass();
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    public function __set(string $key, $value)
    {
        if ($this->hasAccessor('set', $key)) {
            $this->callAccessor('set', $key, $value);

            return;
        }

        if (!BagHelper::isArray($value) || !$this->hasAccessor('add', $key) || !$this->hasAccessor('remove', $key)) {
            if ($this->hasGenericAccessor('set')) {
                $this->callGenericAccessor('set', $key, $value);

                return;
            }

            $this->object->{$key} = $value;

            return;
        }

        $current = BagHelper::toArray($this->__get($key));

        $staying = array_uintersect(
            BagHelper::toArray($current),
            BagHelper::toArray($value),
            function ($a, $b) {
                return strcmp(is_object($a) ? spl_object_hash($a) : $a, is_object($b) ? spl_object_hash($b) : $b);
            }
        );

        foreach ($current as $currentValue) {
            if (!in_array($currentValue, $staying)) {
                $this->callAccessor('remove', $key, $currentValue);
            }
        }

        foreach ($value as $singleValue) {
            if (!in_array($singleValue, $staying)) {
                $this->callAccessor('add', $key, $singleValue);
            }
        }
    }

    public function __get(string $key)
    {
        if ($this->hasAccessor('get', $key)) {
            return $this->callAccessor('get', $key);
        }

        if ($this->hasAccessor('is', $key)) {
            return $this->callAccessor('is', $key);
        }

        if ($this->hasAccessor('has', $key)) {
            return $this->callAccessor('has', $key);
        }

        if ($this->hasGenericAccessor('get')) {
            return $this->callGenericAccessor('get', $key);
        }

        return isset($this->object->{$key}) ? $this->object->{$key} : null;
    }

    protected function hasAccessor(string $type, string $key): bool
    {
        return method_exists($this->object, $type . ucfirst($key));
    }

    protected function callAccessor(string $type, string $key, $value = null)
    {
        return call_user_func([$this->object, $type . ucfirst($key)], $value);
    }

    protected function hasGenericAccessor(string $type): bool
    {
        return method_exists($this->object, $type);
    }

    protected function callGenericAccessor(string $type, string $key, $value = null)
    {
        return call_user_func([$this->object, $type], $key, $value);
    }

    public static function get($object, string $key)
    {
        $formObject = new Accessor($object);

        return $formObject->{$key};
    }

    public static function set($object, string $key, $value): Accessor
    {
        $formObject = new Accessor($object);
        $formObject->{$key} = $value;

        return $formObject;
    }
}
