<?php
namespace Avris\Forms\Security;

use Avris\Http\Session\SessionInterface;

final class CsrfProvider implements CsrfProviderInterface
{
    private const SESSION_KEY = '_csrf';

    /** @var SessionInterface */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getCsrfToken(string $identifier): string
    {
        $tokens = $this->session->get(self::SESSION_KEY);

        if (isset($tokens[$identifier])) {
            return $tokens[$identifier];
        }

        $token = sha1(uniqid(mt_rand(), true));

        $tokens[$identifier] = $token;
        $this->session->set(self::SESSION_KEY, $tokens);

        return $token;
    }
}
