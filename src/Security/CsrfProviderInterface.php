<?php
namespace Avris\Forms\Security;

interface CsrfProviderInterface
{
    public function getCsrfToken(string $identifier): string;
}
