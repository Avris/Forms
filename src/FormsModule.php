<?php
namespace Avris\Forms;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

class FormsModule implements ModuleInterface
{
    use ModuleTrait;
}
