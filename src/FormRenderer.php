<?php
namespace Avris\Forms;

use Avris\Bag\BagHelper;
use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Forms\Style\DefaultStyle;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Forms\Style\FormStyleInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FormRenderer extends AbstractExtension implements EventSubscriberInterface
{
    private static $assetsRendered = [];

    /** @var FormStyleInterface[] */
    private static $styles = [];

    public function getFunctions()
    {
        return [
            new TwigFunction('widget', function (
                Environment $twig,
                array $context,
                Widget\Widget $widget,
                $name = null,
                string $style = null
            ) {
                $context = array_merge($context, [
                    'widget' => $widget,
                    'style' => $this->buildStyle($style ?? $context['style'] ?? DefaultStyle::class),
                    'nameParts' => $this->buildNameParts($context, $name),
                ]);

                $reflection = new \ReflectionClass($widget);
                $classes = [];
                do {
                    $template = 'Form/' . strtr($reflection->getName(), ['\\' => '/']) . '.html.twig';
                    try {
                        return $twig->render($template, $context);
                    } catch (LoaderError $e) {
                        $expectedMessage = sprintf('Unable to find template "%s"', $template);
                        if (mb_strpos($e->getMessage(), $expectedMessage) === false) {
                            throw $e;
                        }

                        $classes[] = $reflection->getName();
                    }
                } while ($reflection = $reflection->getParentClass());

                throw new NotFoundException(sprintf(
                    'Cannot find a template for widget %s',
                    join(' extending ', $classes)
                ));
            }, ['needs_environment' => true, 'needs_context' => true, 'is_safe' => ['html']]),

            new TwigFunction('widgetId', function (array $context) {
                return implode('_', $context['nameParts']);
            }, ['needs_context' => true]),

            new TwigFunction('widgetName', function (array $context) {
                $parts = $context['nameParts'];
                $out = array_shift($parts);
                foreach ($parts as $part) {
                    $out .= '[' . $part . ']';
                }

                return $out;
            }, ['needs_context' => true]),

            new TwigFunction('widgetAssets', function (Environment $twig, string $widgetName): string {
                if (isset(self::$assetsRendered[$widgetName])) {
                    return '';
                }

                self::$assetsRendered[$widgetName] = true;

                if (substr($widgetName, -10) === '.html.twig') {
                    $widgetName = substr($widgetName, 0, -10);
                }

                return $twig->render($widgetName . '_assets.html.twig');
            }, ['needs_environment' => true, 'is_safe' => ['html']]),
        ];
    }

    private function buildStyle($style): FormStyleInterface
    {
        return is_string($style)
            ? (self::$styles[$style] ?? self::$styles[$style] = new $style)
            : $style;
    }

    private function buildNameParts(array $context, $name): array
    {
        $nameParts = $context['nameParts'] ?? [];

        if ($name) {
            foreach (BagHelper::toArray($name) as $namePart) {
                $nameParts[] = $namePart;
            }
        }

        return $nameParts;
    }

    public static function reset()
    {
        self::$assetsRendered = [];
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'request' => function () {
            self::reset();
        };
    }
}
