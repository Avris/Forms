<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class BootstrapInline extends Bootstrap
{
    public function getWrapperBefore(): string
    {
        return '<div>';
    }

    public function getWrapperAfter(): string
    {
        return '</div>';
    }
}
