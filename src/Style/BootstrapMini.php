<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class BootstrapMini extends Bootstrap
{
    public function getWidgetClass(): string
    {
        return 'form-control input-sm';
    }
}
