<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class DefaultStyle implements FormStyleInterface
{
    public function getWrapperBefore(): string
    {
        return '<div>';
    }

    public function getWrapperAfter(): string
    {
        return '</div>';
    }

    public function getWrapperClass(): string
    {
        return '';
    }

    public function getWidgetBefore(): string
    {
        return '';
    }

    public function getWidgetAfter(): string
    {
        return '';
    }

    public function getLabelClass(): string
    {
        return '';
    }

    public function getWidgetClass(): string
    {
        return '';
    }

    public function hasLabel(): bool
    {
        return true;
    }

    public function getParam(string $param, string $default = ''): string
    {
        return $default;
    }
}
