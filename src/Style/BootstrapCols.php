<?php
namespace Avris\Forms\Style;

abstract class BootstrapCols extends Bootstrap
{
    const WIDTH = 12;

    public function getWrapperBefore(): string
    {
        return '<div class="row">';
    }

    public function getWrapperAfter(): string
    {
        return '</div>';
    }

    public function getWidgetBefore(): string
    {
        return '<div class="col-lg-' . static::WIDTH . '">';
    }

    public function getWidgetAfter(): string
    {
        return '</div>';
    }

    public function getLabelClass(): string
    {
        return 'form-label col-lg-' . (12 - static::WIDTH);
    }
}
