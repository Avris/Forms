<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class BootstrapInlineNoLabel extends BootstrapInline
{
    public function hasLabel(): bool
    {
        return false;
    }
}
