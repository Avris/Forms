<?php
namespace Avris\Forms\Style;

interface FormStyleInterface
{
    public function getWrapperBefore(): string;

    public function getWrapperAfter(): string;

    public function getWrapperClass(): string;

    public function getWidgetBefore(): string;

    public function getWidgetAfter(): string;

    public function getLabelClass(): string;

    public function getWidgetClass(): string;

    public function hasLabel(): bool;

    public function getParam(string $param, string $default = ''): string;
}
