<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class Bootstrap implements FormStyleInterface
{
    const PARAMS = [
        'btnAddText' => '<span class="fa fa-plus-circle"></span>',
        'btnAddClass' => 'btn btn-success',
        'btnRemoveText' => '<span class="fa fa-trash"></span>',
        'btnRemoveClass' => 'btn btn-danger',
    ];

    const WIDTH = 12;

    public function getWrapperBefore(): string
    {
        return '<div class="row"><div class="col-lg-12">';
    }

    public function getWrapperAfter(): string
    {
        return '</div></div>';
    }

    public function getWrapperClass(): string
    {
        return 'form-group';
    }

    public function getWidgetBefore(): string
    {
        return '';
    }

    public function getWidgetAfter(): string
    {
        return '';
    }

    public function getLabelClass(): string
    {
        return 'form-label';
    }

    public function getWidgetClass(): string
    {
        return 'form-control';
    }

    public function hasLabel(): bool
    {
        return true;
    }

    public function getParam(string $param, string $default = ''): string
    {
        return static::PARAMS[$param] ?? $default;
    }
}
