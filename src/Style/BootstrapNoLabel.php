<?php
namespace Avris\Forms\Style;

/**
 * @codeCoverageIgnore
 */
class BootstrapNoLabel extends Bootstrap
{
    public function hasLabel(): bool
    {
        return false;
    }
}
