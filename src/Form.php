<?php
namespace Avris\Forms;

use Avris\Bag\Bag;
use Avris\Bag\BagHelper;
use Avris\Http\Request\RequestInterface;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\Widget as Widget;

abstract class Form extends Widget\Widget
{
    private const CSRF_FIELD = '_csrf';

    /** @var Widget\Widget[][] */
    protected $data = [];

    /** @var string */
    protected $currentSection = '';

    /** @var bool */
    protected $bound = false;

    /** @var WidgetFactory */
    private $widgetFactory;

    /** @var CsrfProviderInterface */
    private $csrfProvider;

    /** @var object|null */
    protected $object;

    /** @var string */
    protected $objectName;

    public function __construct(WidgetFactory $widgetFactory, CsrfProviderInterface $csrfProvider)
    {
        $this->widgetFactory = $widgetFactory;
        $this->csrfProvider = $csrfProvider;
    }

    public function initialise(array $options, array $asserts = []): Widget\Widget
    {
        parent::initialise($options, $asserts);

        $this->setCsrfToken();
        $this->configure();

        return $this;
    }

    public function bindObject($object)
    {
        $accessor = new Accessor($object);
        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                $field->bindObject($accessor->{$name});
            }
        }

        $this->object = $object;
        $fqcnParts = explode('\\', get_class($object));
        $this->objectName = end($fqcnParts);

        return $this;
    }

    public function buildObject($object = null)
    {
        $accessor = new Accessor($object ?? $this->object ?? new \stdClass());
        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                if (!$field instanceof Widget\DontMapOnObject && !$field->isReadonly()) {
                    $accessor->{$name} = $field->buildObject($accessor->{$name});
                }
            }
        }

        return $accessor->getObject();
    }

    public function getData()
    {
        $value = [];

        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                $value[$name] = $field->getData();
            }
        }

        return $value;
    }

    protected function setData($data)
    {
        foreach (BagHelper::toArray($data) as $field => $value) {
            if ($this->has($field)) {
                $this->get($field)->setData($value);
            }
        }

        return $this;
    }

    protected function setCsrfToken()
    {
        if ($this->getOption('csrf', true)) {
            $this->add(self::CSRF_FIELD, Widget\Csrf::class, [
                'token' => $this->csrfProvider->getCsrfToken($this->getName()),
            ]);
        }
    }

    abstract protected function configure();

    public function startSection(string $section): self
    {
        $this->currentSection = $section;

        return $this;
    }

    public function endSection(): self
    {
        $this->currentSection = '';

        return $this;
    }

    public function add(
        string $name,
        string $type = Widget\Text::class,
        array $options = [],
        $asserts = []
    ): self {
        return $this->addWidget($name, $this->widgetFactory->build($type, $options, $asserts));
    }

    public function addWidget(string $name, Widget\Widget $widget): self
    {
        if ($this->get($name)) {
            throw new \InvalidArgumentException(sprintf('Cannot redefine widget "%s"', $name));
        }

        if (!isset($this->data[$this->currentSection])) {
            $this->data[$this->currentSection] = [];
        }

        $this->data[$this->currentSection][$name] = $widget;

        return $this;
    }

    public function get(string $name): ?Widget\Widget
    {
        foreach ($this->data as $section) {
            if (isset($section[$name])) {
                return $section[$name];
            }
        }

        return null;
    }

    public function has(string $name): bool
    {
        return $this->get($name) instanceof Widget\Widget;
    }

    public function getName(): string
    {
        return $this->getOption('name', $this->objectName ?? 'form');
    }

    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    public function bindRequest(RequestInterface $request): bool
    {
        $formName = $this->getName();

        if (!$request->isPost()) {
            return $this->bound = false;
        }

        if (!isset($request->getData()[$formName])) {
            return $this->bound = false;
        }

        $data = $this->merge(
            $request->getData()[$formName] ?? [],
            $request->getFiles()[$formName] ?? []
        );

        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                if (!$field instanceof Widget\DontBindRequest && !$field->isReadonly()) {
                    $field->setData($data[$name] ?? null);
                    if ($field instanceof Form) {
                        $field->bound = true;
                    }
                }
            }
        }

        return $this->bound = true;
    }

    private function merge($arr1, $arr2)
    {
        if (!$this->isArray($arr1) || !$this->isArray($arr2)) {
            return $arr2;
        }

        foreach ($arr2 as $key2 => $value2) {
            $arr1[$key2] = $this->merge(
                $arr1[$key2] ?? null,
                $value2
            );
        }

        return $arr1;
    }

    private function isArray($element)
    {
        return is_array($element) || $element instanceof Bag;
    }

    public function isBound(): bool
    {
        return $this->bound;
    }

    public function isValid($ignoreBound = false): bool
    {
        if (!$this->bound && !$ignoreBound) {
            return false;
        }

        $valid = parent::isValid();

        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                if (!$field->isValid()) {
                    $valid = false;
                }
            }
        }

        return $valid;
    }

    public function iterate($start = null, $stop = null)
    {
        $started = !$start;

        foreach ($this->data as $section) {
            foreach ($section as $name => $field) {
                if (!$started && $name === $start) {
                    $started = true;
                }

                if ($started) {
                    yield $name => $field;
                }

                if ($stop && $name === $stop) {
                    return;
                }
            }
        }
    } // @codeCoverageIgnore

    /**
     * @return Widget\Widget[][]
     */
    public function all()
    {
        return $this->data;
    }
}
