<?php
namespace Avris\Forms\Assert;

class NumberTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Number();
        $this->assertInvalidFor('');
        $this->assertValidFor('2');
        $this->assertValidFor('-2');
        $this->assertValidFor('4.5');
        $this->assertValidFor(4.5);
        $this->assertValidFor(0);
        $this->assertInvalidFor('4,5');
        $this->assertInvalidFor('foo');
    }
}