<?php
namespace Avris\Forms\Assert;

class MaxCountTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MaxCount(2);
        $this->assertValidFor('');
        $this->assertValidFor([]);
        $this->assertValidFor([1]);
        $this->assertValidFor([1, 2]);
        $this->assertInvalidFor([1, 2, 3]);

        $this->assertEquals(['%value%' => 2], $this->assert->getReplacements());
    }
}