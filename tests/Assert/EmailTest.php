<?php
namespace Avris\Forms\Assert;

class EmailTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Email();
        $this->assertInvalidFor('');
        $this->assertValidFor('andrzej@avris.it');
        $this->assertInvalidFor('andrzej @avris.it');
        $this->assertInvalidFor('andrzej');
    }
}