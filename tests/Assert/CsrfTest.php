<?php
namespace Avris\Forms\Assert;

class CsrfTest extends AssertTest
{
    public function testAssert()
    {
        if (!session_id()) {
            session_start();
        }

        $this->assert = new Csrf('foo_csrf');
        $this->assertValidFor('foo_csrf');
        $this->assertInvalidFor('ooo');
    }
}
