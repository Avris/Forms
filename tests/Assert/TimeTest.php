<?php
namespace Avris\Forms\Assert;

class TimeTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Time();
        $this->assertInvalidFor('');
        $this->assertValidFor('00:00');
        $this->assertValidFor('15:50');
        $this->assertInvalidFor('15:89');
        $this->assertValidFor('15:33:23');
        $this->assertInvalidFor('foo');

        $this->assertEquals(['pattern' => '^\d{2}:\d{2}$'], $this->assert->getHtmlAttributes());
    }
}