<?php
namespace Avris\Forms\Assert;

use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Tool\Security\CryptInterface;
use PHPUnit\Framework\MockObject\MockObject;

class ValidCredentialsTest extends AssertTest
{
    /** @var CryptInterface|MockObject */
    private $crypt;

    protected function setUp()
    {
        $this->crypt = $this->getMockBuilder(CryptInterface::class)->getMock();
        $this->assert = new ValidCredentials(function () {
            return new MemoryUser('user', [
                'authenticators' => ['password' => 'foo'],
            ]);
        }, $this->crypt);
    }

    public function testUserNotFound()
    {
        $this->crypt->expects($this->never())->method('passwordVerify');

        $this->assert = new ValidCredentials(function () {
            return null;
        }, $this->crypt);

        $this->assertInvalidFor('nope');
    }

    public function testPasswordInvalid()
    {
        $this->crypt->expects($this->once())->method('passwordVerify')->willReturn(false);
        $this->crypt->expects($this->never())->method('passwordNeedsRehash');
        $this->crypt->expects($this->never())->method('passwordHash');

        $this->assertInvalidFor('inv');
    }

    public function testPasswordValidNoRehash()
    {
        $this->crypt->expects($this->once())->method('passwordVerify')->willReturn(true);
        $this->crypt->expects($this->once())->method('passwordNeedsRehash')->willReturn(false);
        $this->crypt->expects($this->never())->method('passwordHash');

        $this->assertValidFor('foo');
    }

    public function testPasswordValidWithRehash()
    {
        $this->crypt->expects($this->once())->method('passwordVerify')->willReturn(true);
        $this->crypt->expects($this->once())->method('passwordNeedsRehash')->willReturn(true);
        $this->crypt->expects($this->once())->method('passwordHash')->with('foo');

        $this->assertValidFor('foo');
    }
}
