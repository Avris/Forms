<?php
namespace Avris\Forms\Assert;

use Avris\Forms\Accessor;
use Avris\Micrus\Model\NoORM;

class UniqueFieldTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new UniqueField('testfield');
        $this->assertValidFor([]);
        $this->assertValidFor([
            ['foo' => 'bar'],
            ['foo' => 'bar'],
        ]);
        $this->assertValidFor([
            ['foo' => 'bar', 'testfield' => 'aaa'],
            ['foo' => 'bar', 'testfield' => 'bbb'],
        ]);
        $this->assertInvalidFor([
            ['foo' => 'bar', 'testfield' => 'aaa'],
            ['foo' => 'bar', 'testfield' => 'aaa'],
        ]);

        $this->assertEquals(['%field%' => 'testfield'], $this->assert->getReplacements());
    }
}
