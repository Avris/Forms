<?php
namespace Avris\Forms\Assert;

class CallbackTestTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Callback(function ($value) {
            return substr($value, 0, 3) === 'xxx' && substr($value, -3) === 'yyy';
        });
        $this->assertValidFor('xxxyyy');
        $this->assertValidFor('xxxabcyyy');
        $this->assertInvalidFor('');
        $this->assertInvalidFor(0);
        $this->assertInvalidFor('1,2');
        $this->assertInvalidFor('foo');
    }
}
