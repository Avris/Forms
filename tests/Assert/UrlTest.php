<?php
namespace Avris\Forms\Assert;

class UrlTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Url();
        $this->assertInvalidFor('');
        $this->assertValidFor('http://avris.it');
        $this->assertValidFor('https://mail.google.com');
        $this->assertValidFor('ftp://hot.xxx');
        $this->assertInvalidFor('avris.it');
        $this->assertInvalidFor('fooo');
    }
}