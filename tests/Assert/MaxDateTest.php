<?php
namespace Avris\Forms\Assert;

class MaxDateTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MaxDate('2015-08-07');
        $this->assertInvalidFor('');
        $this->assertValidFor('2015-08-07');
        $this->assertValidFor('2015-08-01');
        $this->assertInvalidFor('2016-01-01');

        $this->assertEquals(['%value%' => '2015-08-07'], $this->assert->getReplacements());

    }
}