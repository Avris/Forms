<?php
namespace Avris\Forms\Assert;

function l() {
    return 'foo';
}

class RegexpTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Regexp('^te*st');
        $this->assertValidFor('tester');
        $this->assertValidFor('test');
        $this->assertValidFor('tst');
        $this->assertValidFor('teeeest');
        $this->assertValidFor('TEST');

        $this->assertInvalidFor('protest');
        $this->assertInvalidFor('tset');

        $this->assertEquals(['pattern' => '^te*st'], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%pattern%' => '^te*st'], $this->assert->getReplacements());
    }
}
