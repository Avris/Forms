<?php
namespace Avris\Forms\Assert;

class MinTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Min(5);
        $this->assertInvalidFor('');
        $this->assertValidFor('5');
        $this->assertValidFor('6');
        $this->assertValidFor('6.5');
        $this->assertInvalidFor(0);
        $this->assertInvalidFor(4.5);

        $this->assertEquals(['min' => 5], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%value%' => 5], $this->assert->getReplacements());
    }
}