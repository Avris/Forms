<?php
namespace Avris\Forms\Assert;

class MaxLengthTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MaxLength(5);
        $this->assertValidFor('');
        $this->assertValidFor('12345');
        $this->assertValidFor('abcde');
        $this->assertValidFor('abc');
        $this->assertInvalidFor('abcdef');

        $this->assertEquals(['maxLength' => 5], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%value%' => 5], $this->assert->getReplacements());
    }
}