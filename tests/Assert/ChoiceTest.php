<?php
namespace Avris\Forms\Assert;

class ChoiceTest extends AssertTest
{
    public function testAssert()
    {
        $choices = [
            1 => 'un',
            2 => 'do',
            3 => 'tri',
        ];

        $this->assert = new Choice($choices, false);
        $this->assertValidFor(1);
        $this->assertValidFor(2);
        $this->assertValidFor(3);
        $this->assertInvalidFor('');
        $this->assertInvalidFor(0);
        $this->assertInvalidFor([1,2]);
        $this->assertInvalidFor('foo');

        $this->assert = new Choice($choices, true);
        $this->assertValidFor([]);
        $this->assertValidFor([1]);
        $this->assertValidFor([1,2,3]);
        $this->assertValidFor([3,2]);
        $this->assertInvalidFor(1);
        $this->assertInvalidFor('foo');
    }
}