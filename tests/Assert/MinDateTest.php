<?php
namespace Avris\Forms\Assert;

class MinDateTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MinDate('2015-08-07');
        $this->assertValidFor('');
        $this->assertValidFor('2015-08-07');
        $this->assertValidFor('2015-09-13');
        $this->assertInvalidFor('2014-01-01');
        $this->assertEquals(['%value%' => '2015-08-07'], $this->assert->getReplacements());
    }
}