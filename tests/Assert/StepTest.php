<?php
namespace Avris\Forms\Assert;

class StepTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Step(0.05);

        $this->assertValidFor('');
        $this->assertValidFor('2');
        $this->assertValidFor('-2');
        $this->assertValidFor('0.25');
        $this->assertValidFor(-0.75);
        $this->assertValidFor(0);
        $this->assertInvalidFor('4.56');
        $this->assertInvalidFor(-0.01);

        $this->assertEquals(['step' => 0.05], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%value%' => 0.05], $this->assert->getReplacements());
    }
}