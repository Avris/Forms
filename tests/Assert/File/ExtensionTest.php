<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\AssertTest;

class ExtensionTest extends AssertTest
{
    /** @dataProvider provider */
    public function testAssert($given, $accepted, $expected)
    {
        $this->assert = new Extension($accepted);
        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getExtension')->willReturn($given);

        $this->assertValidation($expected, $uploadedFile);
    }

    public function provider()
    {
        return [
            ['jpg', 'jpg', true],
            ['jpeg', 'jpg', false],
            ['jpg', ['jpg', 'jpeg'], true],
            ['jpeg', ['jpg', 'jpeg'], true],
        ];
    }
}