<?php
namespace Avris\Forms\Assert\File;

class MaxWidthTest extends FileAssertTest
{
    const CLS = MaxWidth::class;

    public function imageSizeProvider()
    {
        return [
            [[1000, 600], true],
            [[1001, 600], false],
        ];
    }
}
