<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\AssertTest;

class TypeTest extends AssertTest
{
    /** @dataProvider provider */
    public function testAssert($given, $accepted, $expected)
    {
        $this->assert = new Type($accepted);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getClientMediaType')->willReturn($given);
        $this->assertValidation($expected, $uploadedFile);
    }

    public function provider()
    {
        return [
            ['image/jpeg', 'image/jpeg', true],
            ['image/png', 'image/jpeg', false],
            ['image/jpeg', ['image/jpeg', 'image/png'], true],
            ['image/png', ['image/jpeg', 'image/png'], true],
        ];
    }
}
