<?php
namespace Avris\Forms\Assert\File;

class MaxHeightTest extends FileAssertTest
{
    const CLS = MaxHeight::class;

    public function imageSizeProvider()
    {
        return [
            [[1200, 1000], true],
            [[1200, 1001], false],
        ];
    }
}
