<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\AssertTest;

class MaxSizeTest extends AssertTest
{
    /** @dataProvider provider */
    public function testAssert($given, $accepted, $expected, $displayed)
    {
        $this->assert = new MaxSize($accepted);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getSize')->willReturn($given);
        $this->assertValidation($expected, $uploadedFile);

        $this->assertEquals(['%value%' => $displayed], $this->assert->getReplacements());
    }

    public function provider()
    {
        return [
            [1024, 1024, true, '1.0 kB'],
            [1024, '1k', true, '1.0 kB'],
            [1024, '1 kb', true, '1.0 kB'],
            [1025, 1024, false, '1.0 kB'],
            [1025, '1k', false, '1.0 kB'],
            [1025, '1 kb', false, '1.0 kB'],
        ];
    }

    /** @expectedException \InvalidArgumentException */
    public function testParseException()
    {
        new MaxSize('foobar');
    }
}