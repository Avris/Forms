<?php
namespace Avris\Forms\Assert\File;

class MinWidthTest extends FileAssertTest
{
    const CLS = MinWidth::class;

    public function imageSizeProvider()
    {
        return [
            [[1000, 600], true],
            [[999, 600], false],
        ];
    }
}
