<?php
namespace Avris\Forms\Assert\File;

class MinHeightTest extends FileAssertTest
{
    const CLS = MinHeight::class;

    public function imageSizeProvider()
    {
        return [
            [[1200, 1000], true],
            [[1200, 999], false],
        ];
    }
}
