<?php
namespace Avris\Forms\Assert\File;

use Avris\Bag\Bag;
use Avris\Container\Container;
use Avris\Container\ContainerInterface;
use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\AssertTest;

class ImageTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Image();

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getTmpName')->willReturn(__FILE__);
        $this->assertInvalidFor($uploadedFile);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getTmpName')->willReturn(__DIR__ . '/../../_help/favicon.ico');
        $this->assertValidFor($uploadedFile);
    }
}