<?php
namespace Avris\Forms\Assert\File;

use Avris\Http\Request\UploadedFile;
use Avris\Forms\Assert\AssertTest;

class FileTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new File();

        $this->assertInvalidFor('string');

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getError')->willReturn(5);

        $this->assertInvalidFor($uploadedFile);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getError')->willReturn(0);

        $this->assertValidFor($uploadedFile);
    }
}