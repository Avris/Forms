<?php
namespace Avris\Forms\Assert;

class IntegerTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Integer();
        $this->assertValidFor('2');
        $this->assertValidFor('-2');
        $this->assertInvalidFor('');
        $this->assertInvalidFor('4.5');
        $this->assertInvalidFor('4,5');
        $this->assertInvalidFor('foo');

        $this->assertSame([], $this->assert->getReplacements());
        $this->assertSame([], $this->assert->getHtmlAttributes());
    }
}
