<?php
namespace Avris\Forms\Assert;

use PHPUnit\Framework\TestCase;

abstract class AssertTest extends TestCase
{
    /** @var Assert */
    protected $assert;

    public function assertValidFor($value)
    {
        $this->assertTrue($this->assert->validate($value));
    }

    public function assertInvalidFor($value)
    {
        $this->assertFalse($this->assert->validate($value));
    }

    public function assertValidation($expected, $value)
    {
        if ($expected) {
            $this->assertValidFor($value);
        } else {
            $this->assertInvalidFor($value);
        }
    }
}