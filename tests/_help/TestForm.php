<?php
namespace Avris\Micrus\Test;

use App\Entity\Testie;
use Avris\Forms\Assert as Assert;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;
use Avris\Forms\WidgetFactory;

class TestForm extends Form
{
    public function configure()
    {
        $choices = ['1' => 'jeden', '2' => 'dwa', '3' => 'trzy'];

        $this
            ->startSection('Standard')
            ->add('text', Widget\Text::class)
            ->add('text2', Widget\Text::class)
            ->add('requiredText', Widget\Text::class, [], new Assert\NotBlank)
            ->add('email', Widget\Email::class, ['placeholder' => 'user@domain.eu'], [new Assert\NotBlank, new Assert\Email])
            ->add('agree', Widget\Checkbox::class, [], new Assert\NotBlank())
            ->add('date', Widget\Date::class, ['class' => 'datetimepicker date-big'])
            ->add('datetime', Widget\DateTime::class)
            ->add('time', Widget\Time::class)
            ->add('file', Widget\File::class, [], new Assert\File\Type(['text/plain', 'text/foo']))
            ->add('number', Widget\Number::class)
            ->add('int', Widget\Integer::class)
            ->add('textAddon', Widget\TextAddon::class, ['before' => '$'])
            ->add('numberAddon', Widget\NumberAddon::class, ['after' => '€'])
            ->add('url', Widget\Url::class)
            ->endSection()

            ->startSection('Choice')
            ->add('choiceEmpty', Widget\Choice::class)
            ->add('choiceSN', Widget\Choice::class, [
                'choices' => $choices,
                'add_empty' => true,
            ])
            ->add('choiceMN', Widget\Choice::class, [
                'choices' => $choices,
                'multiple' => true,
                'choiceTranslation' => 'prefix.',
            ])
            ->add('choiceSE', Widget\Choice::class, [
                'choices' => $choices,
                'expanded' => true,
            ])
            ->add('choiceME', Widget\Choice::class, [
                'choices' => $choices,
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('choiceModel', Widget\Choice::class, [
                'choices' => Testie::all(),
                'keys' => function (Testie $testie) {
                    return $testie->getId();
                },
            ])
            ->add('choiceModelM', Widget\Choice::class, [
                'choices' => Testie::all(),
                'keys' => function (Testie $testie) {
                    return $testie->getId();
                },
                'multiple' => true,
            ])

            ->startSection('Advanced')
            ->add('custom', Widget\Custom::class, [
                'label' => '',
                'template' => 'Form/custom',
            ])
            ->add('notes', Widget\Multiple::class, [
                'widget' => Widget\Text::class,
                'element_asserts' => [
                    new Assert\NotBlank(),
                ]
            ], new Assert\MaxCount(3))
            ->add('dependencies', Widget\Multiple::class, [
                'widget' => DependencyForm::class,
                'element_prototype' => new Dependency(),
                'remove' => function (Dependency $dependency) {
                    return $dependency->getString() !== 'DONTREMOVE';
                },
            ], [
                new Assert\UniqueField('option'),
                new Assert\MaxCount(2),
            ])
            ->add('mainDependency', DependencyForm::class, [
                'csrf' => false,
                'prototype' => new Dependency(),
            ])
        ;
    }

    public function triggerDuplicateWidget()
    {
        $this->add('text');
    }

    public function triggerInvalidWidget()
    {
        $this->add('invalid', WidgetFactory::class);
    }

    public function triggerWidgetNoTemplate()
    {
        $this->add('noTemplate', WidgetNoTemplate::class);
    }

    public function triggerBrokenTemplate()
    {
        $this->add('noTemplate', Widget\Custom::class, [
            'template' => 'Form/broken'
        ]);
    }

    public function getName(): string
    {
        return 'Test';
    }
}
