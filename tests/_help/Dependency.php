<?php
namespace Avris\Micrus\Test;

class Dependency
{
    const OPTIONS = [
        'foo' => 'foo',
        'bar' => 'bar',
        'baz' => 'baz',
    ];

    /** @var string */
    private $option;

    /** @var string */
    private $string;

    public function __construct(string $option = 'foo', string $string = '')
    {
        $this->option = $option;
        $this->string = $string;
    }

    public function getOption(): string
    {
        return $this->option;
    }

    public function setOption(string $option): self
    {
        $this->option = $option;

        return $this;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): self
    {
        $this->string = $string;

        return $this;
    }
}
