<?php
namespace Avris\Micrus\Test;

use Avris\Forms\Assert as Assert;
use Avris\Forms\Widget as Widget;
use Avris\Forms\Form;

class DependencyForm extends Form
{
    public function configure()
    {
        $this
            ->add('option', Widget\Choice::class, ['choices' => Dependency::OPTIONS])
            ->add('string', Widget\Text::class, [], [new Assert\NotBlank(), new Assert\MaxLength(100)])
        ;
    }
}
