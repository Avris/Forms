<?php
namespace App\Entity;

final class Testie
{
    private $id;

    private $value;

    public function __construct($id = 0, $value = null)
    {
        $this->id = $id;
        $this->value = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string) $this->value;
    }

    private static $all;

    public static function all()
    {
        if (self::$all) {
            return self::$all;
        }

        return self::$all = [
            new self(7, 'siedem'),
            new self(8, 'osiem'),
            new self(9, 'dziewięć'),
        ];
    }
}
