<?php
namespace Avris\Forms\Test;

use Avris\Http\Session\SessionInterface;

class FakeSession implements SessionInterface
{
    /** @var array */
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function getId(): string
    {
        return 'MEM';
    }

    public function has(string $key): bool
    {
        return isset($this->data[$key]);
    }

    public function get(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }

    public function set(string $key, $value): SessionInterface
    {
        $this->data[$key] = $value;

        return $this;
    }

    public function all(): array
    {
        return $this->data;
    }

    public function delete(string $key): SessionInterface
    {
        unset($this->data[$key]);

        return $this;
    }

    public function writeClose(): SessionInterface
    {
        return $this;
    }

    public function destroy(): SessionInterface
    {
        $this->data = [];

        return $this;
    }

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}
