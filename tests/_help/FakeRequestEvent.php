<?php
namespace Avris\Forms\Test;

use Avris\Dispatcher\Event;

class FakeRequestEvent extends Event
{
    public function getName(): string
    {
        return 'request';
    }

    public function setValue($value): Event
    {
        return $this;
    }

    public function getValue()
    {
        return null;
    }
}
