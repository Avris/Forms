<?php
namespace Avris\Forms;

use App\Entity\Testie;
use Avris\Http\Request\MemoryFile;
use Avris\Http\Request\Request;
use Avris\Http\Session\SessionInterface;
use Avris\Localisator\LocalisatorBuilder;
use Avris\Localisator\LocalisatorExtension;
use Avris\Localisator\LocalisatorInterface;
use Avris\Localisator\LocalisatorTwig;
use Avris\Forms\Widget\Text;
use Avris\Micrus\Test\Dependency;
use Avris\Micrus\Test\DependencyForm;
use Avris\Micrus\Test\TestForm;
use Avris\Micrus\Test\WidgetNoTemplate;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class FormIntegrationTest extends TestCase
{
    /** @var WidgetFactory */
    private static $widgetFactory;

    /** @var Environment */
    private static $twig;

    public static function setUpBeforeClass()
    {
        $builder = (new LocalisatorBuilder())
            ->registerExtension(new LocalisatorExtension('en', [__DIR__ . '/../translations']))
            ->registerExtension(new FormsExtension(
                [TestForm::class => [], DependencyForm::class => []],
                [WidgetNoTemplate::class => []]
            ));

        $localisator = $builder->build(LocalisatorInterface::class);

        $loader = new FilesystemLoader([__DIR__ . '/_help/templates', __DIR__ . '/../templates']);
        self::$twig = new Environment($loader, [
            'cache' => __DIR__ . '/_output/cache',
            'debug' => true,
            'strict_variables' => true,
        ]);
        self::$twig->addExtension(new FormRenderer());
        self::$twig->addExtension(new LocalisatorTwig($localisator));

        self::$widgetFactory = $builder->build(WidgetFactory::class);
        $session = $builder->build(SessionInterface::class);
        $session->set('_csrf', ['Test' => 'CSRF']);
    }

    protected function setUp()
    {
        FormRenderer::reset();
    }

    private function buildForm(): TestForm
    {
        return self::$widgetFactory->build(TestForm::class);
    }

    private function renderForm(TestForm $form): string
    {
        return self::$twig->render('form.html.twig', ['form' => $form]);
    }

    private function assertHtmlFile(string $filename, string $actual)
    {
        $actual = sprintf(
            '<link rel="stylesheet"
                   href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
                   integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy"
                   crossorigin="anonymous" />
             <div class="container">%s</div>',
            $actual
        );

        $this->saveOutput($filename, $actual);

        $this->assertEquals(
            file_get_contents(__DIR__ . '/_help/integration/' . $filename . '.html'),
            str_replace('<option value="" selected>', '<option value="" >', $actual)
        );
    }

    private function saveOutput(string $filename, string $html)
    {
        @mkdir(__DIR__ . '/_output/html', 0777, true);
        file_put_contents(__DIR__ . '/_output/html/' . $filename . '.html', $html);
    }

    private function getDatabaseObject(): \stdClass
    {
        return (object) [
            'text' => 'foo',
            'text2' => 'bar',
            'requiredText' => 'lorem',
            'email' => 'test@trashmail.com',
            'agree' => true,
            'date' => new \DateTime('2014-11-19'),
            'datetime' => new \DateTime('2014-11-19 12:34'),
            'time' => new \DateTime('12:34'),
            'file' => null,
            'number' => 15.3,
            'int' => 8,
            'textAddon' => 49,
            'numberAddon' => 69.99,
            'url' => 'https://micrus.avris.it',

            'choiceEmpty' => '',
            'choiceSN' => '1',
            'choiceMN' => ['2', '3'],
            'choiceSE' => '1',
            'choiceME' => ['2', '3'],
            'choiceModel' => Testie::all()[1],
            'choiceModelM' => [Testie::all()[0], Testie::all()[1]],

            'custom' => 'OK',

            'notes' => ['osiem', 'dwa'],
            'dependencies' => [new Dependency('foo', 'ok'), new Dependency('baz', 'DONTREMOVE')],
            'mainDependency' => new Dependency('bar', 'barrrr!'),
        ];
    }

    public function testEmpty()
    {
        $form = $this->buildForm();

        $html = $this->renderForm($form);

        $this->assertHtmlFile('empty', $html);

        $this->assertFalse($form->isValid());
        $this->assertFalse($form->isBound());

        $this->assertFalse($form->has('nope'));
        $this->assertTrue($form->has('text'));
        $this->assertInstanceOf(Text::class, $form->get('text'));

        $iterated = iterator_to_array($form->iterate('email', 'date'));
        $this->assertCount(3, $iterated);
    }

    public function testObjectBoundEmpty()
    {
        $form = $this->buildForm();

        $obj = new \stdClass;

        $form->bindObject($obj);

        $html = $this->renderForm($form);

        $this->assertHtmlFile('empty', $html);

        $this->assertFalse($form->isValid());
        $this->assertFalse($form->isBound());
    }

    public function testObjectBoundFull()
    {
        $form = $this->buildForm();

        $obj = $this->getDatabaseObject();

        $form->bindObject($obj);

        $html = $this->renderForm($form);

        $this->assertHtmlFile('objectBound', $html);

        $this->assertFalse($form->isValid());
        $this->assertFalse($form->isBound());
    }

    public function testRequestBoundInvalid()
    {
        $form = $this->buildForm();

        $obj = $this->getDatabaseObject();

        $form->bindObject($obj);

        $form->bindRequest(Request::create('POST', '/', [], [], [
            'Test' => [
                'text' => 'FOO',
                'text2' => 'BAR',
                'requiredText' => '',
                'email' => 'foo',
                //'agree' empty
                'date' => 'foo',
                'datetime' => 'foo',
                'time' => 'foo',
                //'file' empty
                'number' => 'foo',
                'int' => 'foo',
                'textAddon' => 49,
                'numberAddon' => 'foo',
                'url' => 'foo',

                'choiceEmpty' => 'foo',
                'choiceSN' => 'foo',
                'choiceMN' => ['foo', '3'],
                'choiceSE' => 'foo',
                'choiceME' => ['foo', '3'],
                'choiceModel' => 'foo',
                'choiceModelM' => ['foo'],

                'notes' => ['', 'foo', 'bar', 'baz'],
                'dependencies' => [
                    '1' => ['option' => 'baz', 'string' => 'DONTREMOVE'],
                    'new0' => ['option' => 'foo', 'string' => 'Fju'],
                    'new1' => ['option' => 'inv', 'string' => 'ok'],
                ],
                'mainDependency' => ['option' => 'bar', 'string' => ''],
            ]
        ], [], [
            'Test' => [
                'name' => ['file' => 'tmp.json'],
                'type' => ['file' => 'application/json'],
                'tmp_name' => ['file' => ''],
                'error' => ['file' => 0],
                'size' => ['file' => 13],
            ],
        ]));

        $this->assertFalse($form->isValid());
        $this->assertTrue($form->isBound());

        $html = $this->renderForm($form);

        $this->assertHtmlFile('requestBoundInvalid', $html);
    }

    public function testBindRequestNotPost()
    {
        $form = $this->buildForm();

        $obj = $this->getDatabaseObject();

        $form->bindObject($obj);

        $form->bindRequest(Request::create('GET', '/', [], [], ['Test' => ['text' => 'ok']]));

        $this->assertFalse($form->isValid());
        $this->assertFalse($form->isBound());
    }

    public function testBindRequestPostNoKey()
    {
        $form = $this->buildForm();

        $obj = $this->getDatabaseObject();

        $form->bindObject($obj);

        $form->bindRequest(Request::create('POST', '/', [], [], ['Invalid' => ['text' => 'ok']]));

        $this->assertFalse($form->isValid());
        $this->assertFalse($form->isBound());
    }

    public function testRequestBoundValid()
    {
        $form = $this->buildForm();

        $obj = $this->getDatabaseObject();

        $form->bindObject($obj);

        $file = new MemoryFile('content', 'text/plain', 'txt');

        $postData = [
            '_csrf' => 'CSRF',
            'text' => 'FOO',
            'text2' => 'BAR',
            'requiredText' => 'LOREM',
            'email' => '  FOO@trashmail.com',
            'agree' => true,
            'date' => '2018-04-01',
            'datetime' => '2018-04-01 13:37',
            'time' => '13:37',
            'file' => $file,
            'number' => 8.8,
            'int' => 8,
            'textAddon' => 69,
            'numberAddon' => 49.99,
            'url' => 'https://avris.it',

            'choiceEmpty' => '',
            'choiceSN' => '2',
            'choiceMN' => ['1', '3'],
            'choiceSE' => '3',
            'choiceME' => ['1'],
            'choiceModel' => '8',
            'choiceModelM' => ['8', '9'],

            'notes' => ['raz', 'dwa', 'trzy'],
            'dependencies' => [
                '1' => ['option' => 'foo', 'string' => 'DONTREMOVE'],
                'new0' => ['option' => 'bar', 'string' => 'Fju'],
            ],
            'mainDependency' => ['option' => 'baz', 'string' => 'bzzZZ!'],
        ];

        $form->bindRequest(Request::create('POST', '/', [], [], [
            'Test' => $postData,
        ]));

        $this->assertTrue($form->isValid());
        $this->assertTrue($form->isBound());

        $form->buildObject($obj);

        $expectedObj = (object) [
            'text' => 'FOO',
            'text2' => 'BAR',
            'requiredText' => 'LOREM',
            'email' => 'foo@trashmail.com',
            'agree' => true,
            'date' => new \DateTime('2018-04-01'),
            'datetime' => new \DateTime('2018-04-01 13:37'),
            'time' => new \DateTime('13:37'),
            'file' => $file,
            'number' => 8.8,
            'int' => 8,
            'textAddon' => 69,
            'numberAddon' => 49.99,
            'url' => 'https://avris.it',

            'choiceEmpty' => '',
            'choiceSN' => '2',
            'choiceMN' => ['1', '3'],
            'choiceSE' => '3',
            'choiceME' => ['1'],
            'choiceModel' => Testie::all()[1],
            'choiceModelM' => [Testie::all()[1], Testie::all()[2]],

            'custom' => 'OK',
            'notes' => ['raz', 'dwa', 'trzy'],
            'dependencies' => [
                new Dependency('foo', 'DONTREMOVE'),
                new Dependency('bar', 'Fju'),
            ],
            'mainDependency' => new Dependency('baz', 'bzzZZ!'),
        ];

        $this->assertEquals($expectedObj, $obj);

        $postData['custom'] = 'OK';
        $postData['dependencies'] = array_values($postData['dependencies']);
        $this->assertEquals($postData, $form->getData());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Cannot redefine widget "text"
     */
    public function testDuplicateWidget()
    {
        $form = $this->buildForm();
        $form->triggerDuplicateWidget();
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Service "Avris\Forms\WidgetFactory" should be an instance of Avris\Forms\Widget\Widget to use it as a form widget
     */
    public function testInvalidWidget()
    {
        $form = $this->buildForm();
        $form->triggerInvalidWidget();
    }

    /**
     * @expectedException \Twig_Error_Runtime
     * @expectedExceptionMessage An exception has been thrown during the rendering of a template ("Cannot find a template for widget Avris\Micrus\Test\WidgetNoTemplate extending Avris\Forms\Widget\Widget").
     */
    public function testWidgetNoTemplate()
    {
        $form = $this->buildForm();
        $form->triggerWidgetNoTemplate();
        $this->renderForm($form);
    }

    /**
     * @expectedException \Twig_Error_Loader
     * @expectedExceptionMessage Unable to find template "nonexistent" (looked into
     */
    public function testWidgetBrokenTemplate()
    {
        $form = $this->buildForm();
        $form->triggerBrokenTemplate();
        $this->renderForm($form);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The File widget must be bound with an instance of Avris\Http\Request\UploadedFile
     */
    public function testWidgetWrongFile()
    {
        $form = $this->buildForm();
        $form->bindRequest(Request::create('POST', '/', [], [], [
            'Test' => [
                'file' => 'foo',
            ],
        ]));
    }
}
