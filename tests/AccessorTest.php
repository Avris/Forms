<?php
namespace Avris\Forms;

use App\Entity\TestObjectApp;
use Avris\Micrus\Test\TestObject;
use Avris\Micrus\Test\TestObjectGen;
use PHPUnit\Framework\TestCase;

class AccessorTest extends TestCase
{
    public function testFormObjectModel()
    {
        $obj = new TestObjectApp();
        $accessor = new Accessor($obj);
        $this->assertSame($obj, $accessor->getObject());
    }

    public function testFormObjectAttributes()
    {
        $accessor = new Accessor();
        $accessor->foo = 'bar';
        $this->assertEquals('bar', $accessor->foo);
        $this->assertInstanceOf('\stdClass', $accessor->getObject());
    }

    public function testFormObjectAccessors()
    {
        $accessor = new Accessor(new TestObject());
        $accessor->foo = 'bar';
        $this->assertEquals('setbarget', $accessor->foo);
        $this->assertTrue($accessor->ok);

        $this->assertInstanceOf(TestObject::class, $accessor->getObject());
    }

    public function testFormObjectAccessorsManyString()
    {
        $accessor = new Accessor(new TestObject());
        TestObject::$removeManyCount = 0;

        $this->assertFalse($accessor->manyFoo);

        $accessor->many = ['foo', 'bar'];
        $this->assertEquals(['foo', 'bar'], array_values($accessor->many));
        $this->assertEquals(0, TestObject::$removeManyCount);

        $accessor->many = ['foo', 'baz'];
        $this->assertEquals(['foo', 'baz'], array_values($accessor->many));
        $this->assertEquals(1, TestObject::$removeManyCount);
        $this->assertTrue($accessor->manyFoo);

        $accessor->many = [];
        $this->assertEquals([], array_values($accessor->many));
        $this->assertEquals(3, TestObject::$removeManyCount);
        $this->assertFalse($accessor->manyFoo);
    }

    public function testFormObjectAccessorsManyObject()
    {
        $accessor = new Accessor(new TestObject());
        TestObject::$removeManyCount = 0;

        $foo = (object) ['val' => 'foo'];
        $bar = (object) ['val' => 'bar'];
        $baz = (object) ['val' => 'baz'];

        $this->assertFalse($accessor->manyFoo);

        $accessor->many = [$foo, $bar];
        $this->assertEquals([$foo, $bar], array_values($accessor->many));
        $this->assertEquals(0, TestObject::$removeManyCount);

        $accessor->many = [$foo, $baz];
        $this->assertEquals([$foo, $baz], array_values($accessor->many));
        $this->assertEquals(1, TestObject::$removeManyCount);
        $this->assertTrue($accessor->manyFoo);

        $accessor->many = [];
        $this->assertEquals([], array_values($accessor->many));
        $this->assertEquals(3, TestObject::$removeManyCount);
        $this->assertFalse($accessor->manyFoo);
    }

    public function testFormObjectGenericAccessors()
    {
        $accessor = new Accessor(new TestObjectGen());
        $accessor->foo = 'bar';
        $this->assertEquals('bar', $accessor->foo);

        $this->assertInstanceOf(TestObjectGen::class, $accessor->getObject());
    }

    public function testStatic()
    {
        $accessor = new \stdClass();

        Accessor::set($accessor, 'foo', 'bar');
        $this->assertEquals('bar', Accessor::get($accessor, 'foo'));
    }
}
