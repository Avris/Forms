<?php

require_once __DIR__ . '/../vendor/autoload.php';

foreach (glob(__DIR__ . '/_help/*.php') as $file) {
    require_once $file;
}

session_start();
