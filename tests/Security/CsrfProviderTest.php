<?php
namespace Avris\Forms\Assert;

use Avris\Forms\Security\CsrfProvider;
use Avris\Forms\Test\FakeSession;
use PHPUnit\Framework\TestCase;

class CsrfProviderTest extends TestCase
{
    public function testProvider()
    {
        $session = new FakeSession(['_csrf' => ['foo' => 'bar']]);

        $provider = new CsrfProvider($session);

        $existingToken = $provider->getCsrfToken('foo');
        $this->assertSame('bar', $existingToken);
        $this->assertSame(['foo' => 'bar'], $session->get('_csrf'));

        $newToken = $provider->getCsrfToken('new');
        $this->assertRegExp('#^[0-9a-f]{40}$#', $newToken);
        $this->assertSame(['foo' => 'bar', 'new' => $newToken], $session->get('_csrf'));
    }
}
